   /7`7  /``7  //``7  //``7
  /7_7   |     |_ 7   \=\
 /7 \\ /__7   __//  \__//
             `               by Juanmi Lopez 

  rs95 is a small demo application made to test the raylib library. It displays
colored bouncing polygons reminiscent of the Windows 95 screensaver Mistify. The
characteristics of the displayed figures are configurable via an arguments file
and some parameters can be manipulated while the application is running.

  The configurable options are:

Number of vertices per polygon: How many vertices each polygon has.
Number of polygons: Each polygon follows the movement of the previous one.
Range of movement vectors: Vertices move linearly across the screen, bouncing
 and changing direction at the borders. Each vertex in the main polygon gets two
 movement vectors (one for each axis), the values of which are numbers inside
 the ranges defined by these arguments randomly picked at launch.
Separation between polygons: How apart are the corresponding vertices from each
 polygon.
Movement scaling factor ("speed"): Value used to scale the movement vectors
 when moving the vertices. This value can be altered in runtime and a default to
 return to is defined. If this value is negative the side of the movement gets
 inverted.
Thickness: Value that defines how thick the vertices and the lines connecting
 them get displayed. This value can be altered in runtime and a default to
 return to is defined. If this value is negative the lines disappear but the
 vertices reappear.
Automatic thickness variation mode: Automatically rises and lowers the thickness
 value within a predefined range. This mode can be toggled on and off during
 runtime.
Thickness variation step: This value defines how much the thickness value
 changes each frame. This value can be altered during runtime and a default to
 return to is defined.
Static color: Three color components (red, green and blue) that define the color
 of the polygons when not in color cycling mode. These values can be altered in
 runtime and a default to return to is defined.
Color cycling mode: Mode in which the colors of the figure change following a
 predefined cycle. This mode can be toggled on and off during runtime.
"Alpha" mode: Value configurable to three possible states (modes) that define
 the alpha (transparency) value for the polygons in the figure. Modes can be
 switched during runtime. The three modes are:
  -Const alpha: All polygons are shown opaque.
  -Front alpha: The first polygon is shown opaque and subsequent polygons are
    shown each one more transparent.
  -Back alpha: The last polygon is shown opaque and the ones in the front are
    shown each one, in order, more transparent.
"Thick" mode: Value configurable to three possible states (modes) that define
 the thickness of each polygon in the figure. The final thickness is relative to
 the general thickness value. Modes can be switched during runtime. The three
 modes are:
  -Const thick: All polygons are shown with the same thickness.
  -Front thick: The first polygon is shown with the general thickness value and
    each subsequent polygon is shown thinner than the previous one.
  -Back thick: The last polygon is shown with the general thickness value and
    the rest are shown thinner as they get closer to the front one.
Finally, the display of some runtime data can also be toggled on and off while
the program is running.


Usage
=====

  rs95 works by reading an arguments file and generating a figure according to
the read arguments. An arguments file can be specified by providing its path
as the first command line argument when launching the program. If the program
is run without providing a file, it will look for a default file named
"rs95.arg" in the same folder as the executable. If said file is not found, the
program will create a figure using built-in default values. Arguments files can
also be provided to rs95 by dragging their icon from the file manager and 
dropping them into the program window. rs95 will load and show the new figure as
soon as the file is dropped.
  Arguments files can be externally modified while the program is running and
reloaded in the program by pessing a key. The new values will be loaded and an
updated figure will be drawn. Even if no values are modified this feature can be
used to reset the current figure.


Arguments file
==============

  Arguments files are just plain text files following a format that rs95 can
understand. These files have many entries, one for each configurable argument.
Each entry goes on a line that starts with the codename of the entry followed
by the value of the entry. There can be whitespace between the codename and the
value. Each entry has one type of value the constraints of which have to be
respected. After the relevant characters that define the value of the entry
nothing is read until the next line, so that space can be used to put in
comments, but there is a limit of characters (currently 1024) for each line. If
a line has more characters than that the reading will fail even if the value
was read successfully. The entries follow a predefined order that has to be
respected, and there can not be empty lines between them. More than one figure
can be defined in an arguments file by appending more sets of entries after the
first one. The sets must not be separated by blank lines. The program will draw
the figures back to front, starting with the first one defined. If bad data is
found in an entry, the reading of the file will be aborted, but previously read
correct figures will be created. The entries are as follow:

Codename   Meaning                                     Valid value
--------   -------                                     -----------

numverts   Number of vertices per polygon              Positive integer value, minimum 3
numpolys   Number of polygons                          Positive integer value, minimum 1
vxmovmin   Minimum value of horizontal movement vector Integer value
vxmovmax   Maximum value of horizontal movement vector Integer value
vymovmin   Minimum value of vertical movement vector   Integer value
vymovmax   Maximum value of vertical movement vector   Integer value
separatn   Separation between each polygon             Floating-point value
speeddft   Default movement scaling value              Floating-point value
thickmin   Minimum thickness                           Floating-point value
thickmax   Maximum thickness                           Floating-point value
thickdft   Default thickness                           Floating-point value
thdelmin   Minimum thickness variation                 Positive floating-point value
thdelmax   Maximum thickness variation                 Positive floating-point value
thdeldft   Default thickness variation                 Positive floating-point value
thickaut   Automatic thickness variation               Boolean value, read as integer, 0 is off any other is on
thicmode   "Thick-mode"                                0 for Const Thick, 1 for Front Thick, 2 for Back Thick
alphmode   "Alpha-mode"                                0 for Const Alpha, 1 for Front Alpha, 2 for Back Alpha
color__r   Default red color component                 Positive integer value, numbers higher than 255 wrap
color__g   Default green color component               Positive integer value, numbers higher than 255 wrap
color__b   Default blue color component                Positive integer value, numbers higher than 255 wrap
clorcycl   Automatic color cycling                     Boolean value, read as integer, 0 is off any other is on

  When loading arguments files, rs95 will check that the format of the file is
compilant and if the values are valid (ie. a minimum is no higher than a
maximum). If it finds any error it will inform the user via stdout, abort the
loading and try to use the "rs95.arg" file. If this file does not exist or is
also invalid, it will finally resort to its built-in defaults. The user is
advised to look into the default file to see a working example, and encouraged
to try different values on files of her own.


Command line arguments
======================

  Some runtime options can be configured using command line arguments when
launching the program.

-h, --help: Will display a short message about the available command line
  options. This argument will be ignored if more arguments are passed.
-d: Use built-in defaults (do not load any arguments file).
-xw=<value>: Will set the canvas width to the amount given by <value>.
-yh=<value>: Will set the canvas height to the amount given by <value>.
-s=<value>: Will scale the canvas by the factor given by <value>.

  Any other data passed via the command line will be interpreted as the path to
an arguments file. Arguments are evaluated just once.


Controls
========

  Use the following keys during runtime to alter figure values:

RIGHT // Control next figure
LEFT  // Control previous figure

  Q   // Change alpha mode
  W   // Change thick mode

  A   // Increase thickness
  Z   // Decrease thickness
  X   // Default thickness
  S   // Automatic thickness variation mode

  D   // Increase thickness variation step
  C   // Decrease thickness variation step
  V   // Set default thickness variation step

  G   // Increase movement scaling
  B   // Decrease movement scaling
  N   // Set default movement scaling value

  E   // Toggle color cycling (the following controls get disabled when color cycling is on)
  4   // Raise the red component of the still color
  R   // Lower the red component of the still color
  5   // Raise the green component of the still color
  T   // Lower the green component of the still color
  6   // Raise the blue component of the still color
  Y   // Lower the blue component of the still color
  3   // Set default still color

  Use the following keys for the corresponding program controls:

  I   // Show live program data
ENTER // Reload current arguments file

  O   // Toggle FPS locking
  P   // Toggle VSYNC


Compilation flags
=================

  If the ARGS_DEBUG macro is defined, the program will output to stdout the
values that it reads from an arguments file when loading it, as it has read them
(before validation). Note that the reading can still fail if the format of the
file or of some value is wrong, in that case the reading will fail and no values
will be shown.

  If the macro KEEPBACKGROUND_OPTION is defined the key K can be used to toggle
background clearing on and off during runtime. When background clearing is off
the program draws on the screen before clearing the previous contents. This
feature can be buggy, that's why it's not standard. It's still cool enough to
check it out so it was kept in the code.

  Both macros are in the source code so they can be commented in an out and
check them without changing the arguments sent to the compiler.


Additional information
======================

  rs95 has been developed and tested using the C (99) programming language on a
Debian based GNU/Linux system running on an x64 machine, using the gcc compiler
version 7.3.0.

  It uses the raylib library version 2.5-dev compiled with the PLATFORM_DESKTOP
flag and the OpenGL 3.3 backend. It also uses the C standard library and one
POSIX function.
