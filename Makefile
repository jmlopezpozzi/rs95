rlcopt = -O1 -Wall -Wextra -std=c99 -pedantic -D_DEFAULT_SOURCE -DPLATFORM_DESKTOP
rllink = -I/usr/local/include -L/usr/local/lib -lraylib -lGL -lm -lpthread -ldl -lrt -lX11 -lglfw
srcd = src/
bind = bin/

#NOTE: Remove -O1 from rlcopt to make Valgrind more useful
#NOTE: Link against GLFW using -lglfw (in rllink) if raylib was compiled without its built-in GLFW

rs95: $(bind)rs95.o $(bind)rmistify.o
	gcc -o rs95 $(bind)rs95.o $(bind)rmistify.o $(rllink)

$(bind)rs95.o: $(srcd)rs95.c $(srcd)rmistify.h
	gcc -c $(rlcopt) -o $(bind)rs95.o $(srcd)rs95.c

$(bind)rmistify.o: $(srcd)rmistify.c $(srcd)rmistify.h
	gcc -c $(rlcopt) -o $(bind)rmistify.o $(srcd)rmistify.c
