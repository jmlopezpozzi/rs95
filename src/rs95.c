/* rs95.c */

#include "rmistify.h"
#include <raylib.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>


#define ARGS_DEBUG  // Define to print argument values read from files
#define KEEPBACKGROUND_OPTION  // Define to enable an option that does not clear previous frames. Cool hack but buggy (and makes showinfo unreadable).

/* Command line arguments constraints */
#define MIN_SCREEN_WIDTH 8
#define MAX_SCREEN_WIDTH 32000
#define MIN_SCREEN_HEIGHT 8
#define MAX_SCREEN_HEIGHT 32000
#define MIN_SCALE 0
#define MAX_SCALE 16
#define MAX_FIGURES 8

/* Defaults */
#define DFT_NUM_VERTS 3
#define DFT_NUM_POLYS 5
#define DFT_SPEED 1.0f
#define DFT_SEPARATION 2.0f
#define DFT_V_MOV_X_MIN -5
#define DFT_V_MOV_X_MAX 5
#define DFT_V_MOV_Y_MIN -5
#define DFT_V_MOV_Y_MAX 5
#define DFT_THICKNESS 1.0f
#define DFT_THICKNESS_MIN 1.0f
#define DFT_THICKNESS_MAX 3.0f
#define DFT_THICK_DELTA 0.02f
#define DFT_THICK_DELTA_MIN 0.005f
#define DFT_THICK_DELTA_MAX 0.5f
#define DFT_ALPHA_MODE RMT_ALPHA_MODE_NO
#define DFT_THICK_MODE RMT_THICK_MODE_CONST
#define DFT_THICK_AUTO false
#define DFT_COLOR (Color){255, 0, 0, 255};
#define DFT_COLOR_R 255
#define DFT_COLOR_G 0
#define DFT_COLOR_B 0
#define DFT_COLORCYCLE true

/* Built-ins */
#define THICK_DELTA_DELTA 0.002f  // Thickness variation step control granularity
#define SPEED_DELTA 0.02f         // Movement scaling control granularity
#define BG_COLOR BLACK
#define FONT_SIZE 20

/* Control keys */
#define KEY_NEXT_FIGURE KEY_RIGHT     // Control next figure
#define KEY_PREV_FIGURE KEY_LEFT      // Control previous figure
#define KEY_ALPHA_TOGGLE KEY_Q        // Change alpha mode
#define KEY_THICK_TOGGLE KEY_W        // Change thick mode
#define KEY_THICK_UP KEY_A            // Increase thickness
#define KEY_THICK_DOWN KEY_Z          // Decrease thickness
#define KEY_THICK_DFT KEY_X           // Default thickness
#define KEY_AUTOTHICK_TOGGLE KEY_S    // Automatic thickness variation mode
#define KEY_THICK_FASTER KEY_D        // Increase thickness variation step
#define KEY_THICK_SLOWER KEY_C        // Decrease thickness variation step
#define KEY_THICK_DELTA_DFT KEY_V     // Set default thickness variation step
#define KEY_SPEED_UP KEY_G            // Increase movement scaling
#define KEY_SPEED_DOWN KEY_B          // Decrease movement scaling
#define KEY_SPEED_DFT KEY_N           // Set default movement scaling value
#define KEY_COLOR_CYCLE_TOGGLE KEY_E  // Toggle color cycling on or off
#define KEY_COLOR_DFT KEY_THREE       // Set the default still color
#define KEY_COLOR_R_UP KEY_FOUR       // Raise the red component of the still color
#define KEY_COLOR_R_DOWN KEY_R        // Lower the red component of the still color
#define KEY_COLOR_G_UP KEY_FIVE       // Raise the green component of the still color
#define KEY_COLOR_G_DOWN KEY_T        // Lower the green component of the still color
#define KEY_COLOR_B_UP KEY_SIX        // Raise the blue component of the still color
#define KEY_COLOR_B_DOWN KEY_Y        // Lower the blue component of the still color
#define KEY_KEEPBACKGROUND KEY_K      // Toggle backround clearing (only works if KEEPBACKGROUND_OPTION is defined)
#define KEY_INFO_TOGGLE KEY_I         // Show live program data
#define KEY_FPS_LOCK_TOGGLE KEY_O     // Toggle FPS locking
#define KEY_VSYNC_TOGGLE KEY_P        // Toggle VSYNC
#define KEY_RELOAD KEY_ENTER          // Reload arguments file

#define VSYNC_ON_FLAGS FLAG_WINDOW_UNDECORATED | FLAG_VSYNC_HINT
#define VSYNC_OFF_FLAGS FLAG_WINDOW_UNDECORATED


struct {
	int window_width;
	int window_height;
	int canvas_width;
	int canvas_height;
	int scale;
	bool undecorated;
	bool vsync;
	bool fpslock;
	bool showinfo;
#ifdef KEEPBACKGROUND_OPTION
	bool clearbackground;
#endif
} display;

static RenderTexture2D fb_tex;  // Used when display.scale > 1


static int get_cmd_arg(char *argv, char *arg_prefix, unsigned min_val,
                       unsigned max_val, int *arg_var);
static int update_filename(char **filename_ptr, char *filename_src);
static int rs95(char *filename);
static void init_display(void (**render_func)(struct rmt_figure **, int, int));
static void close_display(void);
static int initialize_figures(char **filename_ptr, struct rmt_figure ***figures,
                              int *num_figures);
static int get_args_from_file(char **filename_ptr, struct rmt_args ***args,
                              int *num_figures);
static FILE *open_args_file(char **filename_ptr);
static int read_figure_info(FILE *fp, struct rmt_args *figure_args);
static struct rmt_args *get_default_args(void);
static bool control(struct rmt_figure **figures, unsigned *control_figure,
                    int num_figures, char **filename_ptr);
static void thickness_control(struct rmt_figure *figure);
static void color_control(struct rmt_figure *figure);
static void window_control(void);
static void render(struct rmt_figure **figures, int num_figures,
                   int control_figure);
static void render_scale(struct rmt_figure **figures, int num_figures,
                         int control_figure);
static void display_figure_info(struct rmt_figure **figures, int control_figure);


#define HELP_MSG \
	"usage: rs95 [<filename> / -d] [-xw=<value>, -yh=<value> -s=<value>]\n" \
	"  <filename>: the path of a file to load figure arguments from\n" \
	"  -d: use built-in defaults\n" \
	"  -xw: set canvas width\n" \
	"  -yh: set canvas height\n" \
	"  -s: set canvas scaling factor\n" \
	"  -h, --help: print this message\n" \
	"if no option is set figure arguments get loaded from %s by default\n" \
	"if said file does not exist the built-in defaults are used\n"  // Pair the %s with DFT_FILENAME

#define DFT_FILENAME "rs95.arg"

int main(int argc, char *argv[])
{
	struct {
		unsigned char d: 1;
		unsigned char xw: 1;
		unsigned char yh: 1;
		unsigned char s: 1;
	} args_set = {
		.d = 0, .xw = 0, .yh = 0, .s = 0,
	};

	char *filename = NULL;

	if (argc == 2 && (strcmp(argv[1], "-h") == 0 ||
	                  strcmp(argv[1], "--h") == 0))
	{
		printf(HELP_MSG, DFT_FILENAME);
		return EXIT_SUCCESS;
	}

	for (int i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "-d") == 0 && args_set.d == 0) {
			filename = NULL;
			args_set.d = 1;
		}
		else
		if (strncmp(argv[i], "-xw=", strlen("-xw=")) == 0 && args_set.xw == 0) {
			get_cmd_arg(argv[i], "-xw=", MIN_SCREEN_WIDTH, MAX_SCREEN_WIDTH,
			            &display.canvas_width);
			args_set.xw = 1;
		}
		else
		if (strncmp(argv[i], "-yh=", strlen("-yh=")) == 0 && args_set.yh == 0) {
			get_cmd_arg(argv[i], "-yh=", MIN_SCREEN_HEIGHT, MAX_SCREEN_HEIGHT,
			            &display.canvas_height);
			args_set.yh = 1;
		}
		else
		if (strncmp(argv[i], "-s=", strlen("-s=")) == 0 && args_set.s == 0) {
			get_cmd_arg(argv[i], "-s=", MIN_SCALE, MAX_SCALE,
			            &display.scale);
			args_set.s = 1;
		}
		else
		if (filename == NULL && args_set.d == 0) {
			update_filename(&filename, argv[i]);
		}
	}

	if (filename == NULL && args_set.d == 0) {
		update_filename(&filename, DFT_FILENAME);
	}

	int ret_val = rs95(filename);  // Valgrind works better with this

	return ret_val;
}

#define MAX_ARG_PREFIX_LEN 16
static int get_cmd_arg(char *argv, char *arg_prefix, unsigned min_val,
                       unsigned max_val, int *arg_var)
{
	unsigned long result;
	char *endptr;
	size_t prefix_len = strnlen(arg_prefix, MAX_ARG_PREFIX_LEN + 1);

	if (prefix_len > MAX_ARG_PREFIX_LEN) {
		fprintf(stderr, "Invalid argument prefix length\n");
		return 1;
	}

	errno = 0;
	result = strtoul(argv + prefix_len, &endptr, 0);

	printf("result for %s is %lu\n", arg_prefix, result);  // Debug

	if (errno != 0 || endptr == argv + prefix_len) {
		fprintf(stderr, "Error reading argument %s\n", arg_prefix);
		return 2;
	}

	if (result < min_val || result > max_val) {
		fprintf(stderr, "Value read for argument %s is out of range\n",
		                arg_prefix);
		return 3;
	}

	*arg_var = (int) result;

	return 0;
}

static int update_filename(char **filename_ptr, char *filename_src)
{
	if (*filename_ptr != NULL) {
		free(*filename_ptr);
		*filename_ptr = NULL;
	}

	if (filename_src == NULL) {
		return 0;
	}

	size_t len = strnlen(filename_src, FILENAME_MAX - 1);  // This is a POSIX function
	char *new_filename = malloc(len + 1);

	if (new_filename == NULL) {
		return 1;
	}

	strncpy(new_filename, filename_src, len);
	new_filename[len] = '\0';
	*filename_ptr = new_filename;

	return 0;
}

static int rs95(char *filename)
{
	struct rmt_figure **figures;  // Will store a dynamic array
	int num_figures = 0;
	unsigned control_figure = 0;
	bool reload = false;
	void (*render_func)(struct rmt_figure **, int, int);
	int ret_val = EXIT_SUCCESS;

	init_display(&render_func);

	// Main loop
	while (!WindowShouldClose()) {

		if (initialize_figures(&filename, &figures, &num_figures) != 0) {
			ret_val = EXIT_FAILURE;

			break;
		}

		while (!WindowShouldClose() && !reload) {
			reload = control(figures, &control_figure, num_figures, &filename);

			for (int i = 0; i < num_figures; ++i) {
				rmt_update(figures[i]);
			}

			(*render_func)(figures, num_figures, control_figure);
		}

		for (int i = 0; i < num_figures; ++i) {
			rmt_destroy(figures[i]);
		}
		free(figures);
		reload = false;
	}

	close_display();

	if (filename != NULL) {
		free(filename);
	}

	return ret_val;
}

static void init_display(void (**render_func)(struct rmt_figure **, int, int))
{
#ifdef KEEPBACKGROUND_OPTION
	display.clearbackground = true;
#endif
	display.vsync = true;
	display.fpslock = true;

	if (display.scale == 0) {
		display.scale = 1;
	}

	if (display.canvas_width == 0 && display.canvas_height == 0) {
		display.scale = 1;
		display.undecorated = true;
	}
	else {
		display.undecorated = false;
	}

	if (display.scale == 1) {
		display.window_width = display.canvas_width;
		display.window_height = display.canvas_height;
		*render_func = render;
	}
	else {
		display.window_width = display.canvas_width * display.scale;
		display.window_height = display.canvas_height * display.scale;
		*render_func = render_scale;
	}

	SetConfigFlags((FLAG_VSYNC_HINT * display.vsync) |
	               (FLAG_WINDOW_UNDECORATED * display.undecorated));

	InitWindow(display.window_width, display.window_height, "rs95");
	SetTargetFPS(60);

	if (display.window_width == 0) {
		display.window_width = GetScreenWidth();
		display.canvas_width = display.window_width;
	}
	if (display.window_height == 0) {
		display.window_height = GetScreenHeight();
		display.canvas_height = display.window_height;
	}

	if (display.scale > 1) {
		fb_tex = LoadRenderTexture(display.canvas_width, display.canvas_height);
	}
}

static void close_display(void)
{
	UnloadRenderTexture(fb_tex);  // It's OK even when no RenderTexture2D was loaded
	CloseWindow();
}

static int initialize_figures(char **filename_ptr, struct rmt_figure ***figures,
                              int *num_figures)
{
	struct rmt_args **args = NULL;  // Will hold a dynamic array of pointers
	bool use_defaults = false;
	int read_figures;

	if (get_args_from_file(filename_ptr, &args, num_figures) != 0) {
		TraceLog(LOG_INFO, "Setting built-in default values");
		*num_figures = 1;
		use_defaults = true;
	}

	read_figures = *num_figures;
	*figures = malloc(sizeof(**figures) * read_figures);

	if (*figures == NULL) {
		TraceLog(LOG_WARNING, "Could not create figures array");

		if (args != NULL) {  // args will be NULL if the defaults were set
			for (int i = 0; i < read_figures; ++i) {
				free(args[i]);
			}
			free(args);
		}

		return 1;
	}

	if (use_defaults == false) {
		for (int i = 0; i < read_figures; ++i) {
			(*figures)[i] = rmt_create(args[i]);

			if ((*figures)[i] == NULL) {
				TraceLog(LOG_WARNING, TextFormat("Error creating figure %d", i));
				*num_figures = i;
				if (i == 0) {
					TraceLog(LOG_INFO, "Setting built-in default values");
					*num_figures = 1;
					use_defaults = true;
				}
				// realloc could be used to shrink *figures here
				break;
			}
		}

		for (int i = 0; i < read_figures; ++i) {
			free(args[i]);
		}
		free(args);
	}

	if (use_defaults == true) {
		(*figures)[0] = rmt_create(get_default_args());

		if ((*figures)[0] == NULL) {
			TraceLog(LOG_WARNING, "Error creating default figure");
		}
	}

	if ((*figures)[0] == NULL) {
		free(*figures);
		return 2;
	}

	return 0;
}

/* num_figures could be the return value, and use negatives for errors, but
   it is not consistent with the rest of the program (positives are read as
   number of errors) */
static int get_args_from_file(char **filename_ptr, struct rmt_args ***args,
                              int *num_figures)
{
	if (*filename_ptr == NULL) {  // Wants the built-in defaults
		return 1;
	}

	FILE *fp;
	struct rmt_args *figure_args;
	struct rmt_args **args_array;
	int read_figures = 0;

	fp = open_args_file(filename_ptr);
	if (fp == NULL) {
		return 2;
	}

	while (feof(fp) == false) {
		if (read_figures >= MAX_FIGURES) {
			TraceLog(LOG_WARNING,
			         TextFormat("Maximum number of figures (%d) read,"
			                    " won't read any further", MAX_FIGURES));
			break;
		}
		TraceLog(LOG_INFO, TextFormat("--Reading figure %d", read_figures));  // Debug

		figure_args = malloc(sizeof(*figure_args));

		if (figure_args == NULL) {
			TraceLog(LOG_WARNING, TextFormat("Could not allocate memory for figure %d arguments", read_figures));
			break;
		}

		if (read_figure_info(fp, figure_args) != 0) {
			free(figure_args);
			break;
		}

		++read_figures;
		args_array = realloc(*args, read_figures * sizeof(*args_array));

		if (args_array == NULL) {
			TraceLog(LOG_WARNING, "Reallocation failed");
			break;
		}

		*args = args_array;
		(*args)[read_figures - 1] = figure_args;
	}

	fclose(fp);
	*num_figures = read_figures;

	if (read_figures == 0) {
		return 3;
	}

	return 0;
}

static FILE *open_args_file(char **filename_ptr)
{
	FILE *fp;

	if (strcmp(*filename_ptr, DFT_FILENAME) != 0) {
		fp = fopen(*filename_ptr, "rt");

		if (fp == NULL) {
			TraceLog(LOG_WARNING, TextFormat("Can't open file %s", *filename_ptr));
			update_filename(filename_ptr, DFT_FILENAME);
		}
	}

	if (strcmp(*filename_ptr, DFT_FILENAME) == 0) {
		fp = fopen(DFT_FILENAME, "rt");

		if (fp == NULL) {
			TraceLog(LOG_WARNING, TextFormat("Can't open file %s", DFT_FILENAME));
			update_filename(filename_ptr, NULL);
			return NULL;
		}
	}

	if (*filename_ptr != NULL) {
		TraceLog(LOG_INFO, TextFormat("Using file %s", *filename_ptr));
	}

	return fp;
}

#define MAX_LINE_LEN 1024
static int read_figure_info(FILE *fp, struct rmt_args *figure_args)
{
	static const struct {
		char *name;
		enum {INT_TYPE, FLT_TYPE} type;
		ptrdiff_t member;
	} arginfos[] = {
		{.name = "numverts", .type = INT_TYPE, .member = offsetof(struct rmt_args, numverts)},
		{.name = "numpolys", .type = INT_TYPE, .member = offsetof(struct rmt_args, numpolys)},
		{.name = "vxmovmin", .type = INT_TYPE, .member = offsetof(struct rmt_args, vxmovmin)},
		{.name = "vxmovmax", .type = INT_TYPE, .member = offsetof(struct rmt_args, vxmovmax)},
		{.name = "vymovmin", .type = INT_TYPE, .member = offsetof(struct rmt_args, vymovmin)},
		{.name = "vymovmax", .type = INT_TYPE, .member = offsetof(struct rmt_args, vymovmax)},
		{.name = "separatn", .type = FLT_TYPE, .member = offsetof(struct rmt_args, separatn)},
		{.name = "speeddft", .type = FLT_TYPE, .member = offsetof(struct rmt_args, speeddft)},
		{.name = "thickmin", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thickmin)},
		{.name = "thickmax", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thickmax)},
		{.name = "thickdft", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thickdft)},
		{.name = "thdelmin", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thdelmin)},
		{.name = "thdelmax", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thdelmax)},
		{.name = "thdeldft", .type = FLT_TYPE, .member = offsetof(struct rmt_args, thdeldft)},
		{.name = "thickaut", .type = INT_TYPE, .member = offsetof(struct rmt_args, thickaut)},
		{.name = "thicmode", .type = INT_TYPE, .member = offsetof(struct rmt_args, thicmode)},
		{.name = "alphmode", .type = INT_TYPE, .member = offsetof(struct rmt_args, alphmode)},
		{.name = "color__r", .type = INT_TYPE, .member = offsetof(struct rmt_args, color__r)},
		{.name = "color__g", .type = INT_TYPE, .member = offsetof(struct rmt_args, color__g)},
		{.name = "color__b", .type = INT_TYPE, .member = offsetof(struct rmt_args, color__b)},
		{.name = "clorcycl", .type = INT_TYPE, .member = offsetof(struct rmt_args, clorcycl)},
	};

	const ptrdiff_t figure_args_addr = (ptrdiff_t) &(*figure_args);
	char read_line[MAX_LINE_LEN];
	char *endptr;
	long intval;
	double fltval;

	for (size_t i = 0; i < sizeof(arginfos) / sizeof(arginfos[0]); ++i) {
		size_t slen = strlen(arginfos[i].name);

		if (fgets(read_line, MAX_LINE_LEN, fp) == NULL) {
			TraceLog(LOG_WARNING, TextFormat("Error reading line %zu", i));
			return 3;
		}

		if (strncmp(read_line, arginfos[i].name, slen) != 0) {
			TraceLog(LOG_WARNING, TextFormat("Bad argname for arg %zu", i));
			return 4;
		}

		if (arginfos[i].type == INT_TYPE) {
			long *member_ptr = (long *)(figure_args_addr + arginfos[i].member);

			errno = 0;
			intval = strtol(&read_line[slen], &endptr, 0);

			if (endptr == &read_line[slen] || errno != 0) {
				TraceLog(LOG_WARNING, TextFormat("Error reading argument %zu", i));
				return 5;
			}

			*member_ptr = intval;
		}
		else {  // FLT_TYPE
			double *member_ptr =
				(double *)(figure_args_addr + arginfos[i].member);

			errno = 0;
			fltval = strtod(&read_line[slen], &endptr);

			if (endptr == &read_line[slen] || errno != 0) {
				TraceLog(LOG_WARNING, TextFormat("Error reading argument %zu", i));
				return 6;
			}

			*member_ptr = fltval;
		}
	}

	figure_args->canvas_w = display.canvas_width;
	figure_args->canvas_h = display.canvas_height;

#ifdef ARGS_DEBUG
	for (size_t i = 0; i < sizeof(arginfos) / sizeof(arginfos[0]); ++i) {

		if (arginfos[i].type == INT_TYPE) {
			long *member_ptr = (long *)(figure_args_addr + arginfos[i].member);
			TraceLog(LOG_INFO, TextFormat("%s value %ld", arginfos[i].name,
			                              *member_ptr));
		}
		else {
			double *member_ptr =
				(double *)(figure_args_addr + arginfos[i].member);
			TraceLog(LOG_INFO, TextFormat("%s value %f", arginfos[i].name,
			                              *member_ptr));
		}
	}
#endif

	return 0;
}

static struct rmt_args *get_default_args(void)
{
	static struct rmt_args default_args = {
		.numverts = DFT_NUM_VERTS,
		.numpolys = DFT_NUM_POLYS,
		.vxmovmin = DFT_V_MOV_X_MIN,
		.vxmovmax = DFT_V_MOV_X_MAX,
		.vymovmin = DFT_V_MOV_Y_MIN,
		.vymovmax = DFT_V_MOV_Y_MAX,
		.separatn = DFT_SEPARATION,
		.speeddft = DFT_SPEED,
		.thickmin = DFT_THICKNESS_MIN,
		.thickmax = DFT_THICKNESS_MAX,
		.thickdft = DFT_THICKNESS,
		.thdelmin = DFT_THICK_DELTA_MIN,
		.thdelmax = DFT_THICK_DELTA_MAX,
		.thdeldft = DFT_THICK_DELTA,
		.thickaut = DFT_THICK_AUTO,
		.thicmode = DFT_THICK_MODE,
		.alphmode = DFT_ALPHA_MODE,
		.color__r = DFT_COLOR_R,
		.color__g = DFT_COLOR_G,
		.color__b = DFT_COLOR_B,
		.clorcycl = DFT_COLORCYCLE,
	};

	default_args.canvas_w = display.canvas_width;
	default_args.canvas_h = display.canvas_height;

	return &default_args;
}

/* Returns true if arguments file has to be reloaded */
static bool control(struct rmt_figure **figures, unsigned *control_figure,
                    int num_figures, char **filename_ptr)
{
	struct rmt_figure *figure = figures[*control_figure];

	// Presentation options
	if (IsKeyPressed(KEY_INFO_TOGGLE)) {
		display.showinfo = 1 - display.showinfo;
	}

#ifdef KEEPBACKGROUND_OPTION
	if (IsKeyPressed(KEY_KEEPBACKGROUND)) {
		display.clearbackground = 1 - display.clearbackground;
	}
#endif

	// Alpha control
	if (IsKeyPressed(KEY_ALPHA_TOGGLE)) {
		rmt_set_alpha_mode(figure, rmt_get_alpha_mode(figure) + 1);
	}

	// Speed control
	if (IsKeyDown(KEY_SPEED_UP)) {
		rmt_change_speed(figure, SPEED_DELTA);
	}
	if (IsKeyDown(KEY_SPEED_DOWN)) {
		rmt_change_speed(figure, -SPEED_DELTA);
	}
	if (IsKeyPressed(KEY_SPEED_DFT)) {
		rmt_set_default_speed(figure);
	}

	// Complex control functions
	thickness_control(figure);
	color_control(figure);
	window_control();

	// Control figure change
	if (IsKeyPressed(KEY_NEXT_FIGURE)) {
		*control_figure = (*control_figure + 1) % num_figures;
	}
	if (IsKeyPressed(KEY_PREV_FIGURE)) {
		if (*control_figure == 0) {
			*control_figure = num_figures - 1;
		}
		else {
			--*control_figure;
		}
	}

	// Arguments file reloading
	if (IsFileDropped()) {
		char **dropped_files;
		int dropped_files_count;

		dropped_files = GetDroppedFiles(&dropped_files_count);
		TraceLog(LOG_INFO, TextFormat("File %s dropped into application",
		                              dropped_files[0]));
		if (dropped_files_count > 1) {
			TraceLog(LOG_INFO, "Only the first detected file will be loaded");
		}

		update_filename(filename_ptr, dropped_files[0]);
		ClearDroppedFiles();
		*control_figure = 0;

		return true;
	}

	if (IsKeyPressed(KEY_RELOAD)) {
		TraceLog(LOG_INFO, "Argument reload requested");
		*control_figure = 0;

		return true;
	}

	return false;
}

static void thickness_control(struct rmt_figure *figure)
{
	if (IsKeyPressed(KEY_THICK_TOGGLE)) {
		rmt_set_thick_mode(figure, rmt_get_thick_mode(figure) + 1);
	}

	if (IsKeyPressed(KEY_AUTOTHICK_TOGGLE)) {
		rmt_toggle_autothick(figure);
	}

	if (IsKeyDown(KEY_THICK_FASTER)) {
		rmt_change_thick_control_delta(figure, THICK_DELTA_DELTA);
	}
	if (IsKeyDown(KEY_THICK_SLOWER)) {
		rmt_change_thick_control_delta(figure, -THICK_DELTA_DELTA);
	}
	if (IsKeyDown(KEY_THICK_DELTA_DFT)) {
		rmt_set_default_thick_control_delta(figure);
	}

	if (rmt_autothick_active(figure) == false) {
		if (IsKeyDown(KEY_THICK_UP)) {
			rmt_thickness_up(figure);
		}
		if (IsKeyDown(KEY_THICK_DOWN)) {
			rmt_thickness_down(figure);
		}
		if (IsKeyPressed(KEY_THICK_DFT)) {
			rmt_set_default_thickness(figure);
		}
	}
}

static void color_control(struct rmt_figure *figure)
{
	if (IsKeyPressed(KEY_COLOR_CYCLE_TOGGLE)) {
		rmt_toggle_colorcycle(figure);
	}

	if (rmt_colorcycle_active(figure) == false) {
		Color cur_color = rmt_get_color(figure);

		if (IsKeyDown(KEY_COLOR_R_DOWN)) {
			if (cur_color.r > 0) {
				--cur_color.r;
			}
		}
		if (IsKeyDown(KEY_COLOR_R_UP)) {
			if (cur_color.r < 255) {
				++cur_color.r;
			}
		}

		if (IsKeyDown(KEY_COLOR_G_DOWN)) {
			if (cur_color.g > 0) {
				--cur_color.g;
			}
		}
		if (IsKeyDown(KEY_COLOR_G_UP)) {
			if (cur_color.g < 255) {
				++cur_color.g;
			}
		}

		if (IsKeyDown(KEY_COLOR_B_DOWN)) {
			if (cur_color.b > 0) {
				--cur_color.b;
			}
		}
		if (IsKeyDown(KEY_COLOR_B_UP)) {
			if (cur_color.b < 255) {
				++cur_color.b;
			}
		}

		if (IsKeyPressed(KEY_COLOR_DFT)) {
			rmt_set_default_color(figure);
		}
		else {
			rmt_set_color(figure, cur_color);  // Should we only do it if the color was changed?
		}
	}
}

static void window_control(void)
{
	if (IsKeyPressed(KEY_FPS_LOCK_TOGGLE)) {
		display.fpslock = 1 - display.fpslock;

		if (display.fpslock == true) {
			SetTargetFPS(60);
		}
		else {
			SetTargetFPS(0);
		}
	}

	if (IsKeyPressed(KEY_VSYNC_TOGGLE)) {
		display.vsync = 1 - display.vsync;

		if (display.scale > 1) {
			UnloadRenderTexture(fb_tex);
		}

		CloseWindow();
		SetConfigFlags((FLAG_VSYNC_HINT * display.vsync) |
		               (FLAG_WINDOW_UNDECORATED * display.undecorated));
		InitWindow(display.window_width, display.window_height, "rs95");

		if (display.scale > 1) {
			fb_tex = LoadRenderTexture(display.canvas_width,
			                           display.canvas_height);
		}

		TraceLog(LOG_INFO,
		         TextFormat("Window control values: vsync = %d, lockedfps = %d",
		                    display.vsync, display.fpslock));
	}
}

static void render(struct rmt_figure **figures, int num_figures,
                   int control_figure)
{
	BeginDrawing();
#ifdef KEEPBACKGROUND_OPTION
	if (display.clearbackground)
#endif
	ClearBackground(BG_COLOR);

	for (int i = 0; i < num_figures; ++i) {
		rmt_render(figures[i]);
	}

	if (display.showinfo == true) {
		display_figure_info(figures, control_figure);
	}

	EndDrawing();
}

static void render_scale(struct rmt_figure **figures, int num_figures,
                         int control_figure)
{
	BeginDrawing();
	BeginTextureMode(fb_tex);

#ifdef KEEPBACKGROUND_OPTION
	if (display.clearbackground)
#endif
	ClearBackground(BG_COLOR);

	for (int i = 0; i < num_figures; ++i) {
		rmt_render(figures[i]);
	}

	EndTextureMode();

	DrawTexturePro(fb_tex.texture,
                   (Rectangle){0.0f, 0.0f, fb_tex.texture.width, -fb_tex.texture.height},
	               (Rectangle){0.0f, 0.0f, display.window_width, display.window_height},
	               (Vector2){0.0f, 0.0f}, 0.0f, WHITE);

	if (display.showinfo == true) {
		display_figure_info(figures, control_figure);
	}

	EndDrawing();
}

static void display_figure_info(struct rmt_figure **figures, int control_figure)
{
	struct rmt_figure *figure = figures[control_figure];
	Color color = rmt_get_color(figure);

	DrawText(TextFormat("fps="), 0, 0, FONT_SIZE, RAYWHITE);
	DrawFPS(MeasureText("fps=", FONT_SIZE), 0);
	DrawText(TextFormat("FIGURE #%u", control_figure), 0, 30, FONT_SIZE, RAYWHITE);
	DrawText(TextFormat("r=%d, g=%d, b=%d", color.r, color.g, color.b),
	         0, 60, FONT_SIZE, RAYWHITE);
	DrawText(TextFormat("colorcycle=%d\nalphamode=%d\nthickmode=%d\n"
	                    "autothick=%d\nthickness=%f\nthick_delta=%f\n"
	                    "speed=%f",
	                    rmt_colorcycle_active(figure),
	                    rmt_get_alpha_mode(figure),
	                    rmt_get_thick_mode(figure),
	                    rmt_autothick_active(figure),
	                    rmt_get_thickness(figure),
	                    rmt_get_thick_control_delta(figure),
	                    rmt_get_speed(figure)),
	         0, 90, FONT_SIZE, RAYWHITE);
}