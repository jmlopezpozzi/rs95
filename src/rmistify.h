/* rmistify.h */

#ifndef RMISTIFY_H
#define RMISTIFY_H

#include <raylib.h>
#include <stdbool.h>


#define COLOR_CYCLING_COMPONENT_CLAMP 255  // Must be between 0 and 255, determines upper value for each color component when color cycling

struct rmt_figure;

struct rmt_args {
	long canvas_w;
	long canvas_h;
	long numverts;
	long numpolys;
	long vxmovmin;
	long vxmovmax;
	long vymovmin;
	long vymovmax;
	double separatn;
	double speeddft;
	double thickmin;
	double thickmax;
	double thickdft;
	double thdelmin;
	double thdelmax;
	double thdeldft;
	long thickaut;
	long thicmode;
	long alphmode;
	long color__r;
	long color__g;
	long color__b;
	long clorcycl;
};

enum rmt_alpha_modes {
	RMT_ALPHA_MODE_NO = 0, RMT_ALPHA_MODE_FRONT, RMT_ALPHA_MODE_BACK,
	RMT_NUM_ALPHA_MODES,
};

enum rmt_thick_modes {
	RMT_THICK_MODE_CONST = 0, RMT_THICK_MODE_FRONT, RMT_THICK_MODE_BACK,
	RMT_NUM_THICK_MODES,
};


int rmt_validate_args(const struct rmt_args *args);
struct rmt_figure *rmt_create(const struct rmt_args *args);
void *rmt_destroy(struct rmt_figure *figure);
void rmt_update(struct rmt_figure *figure);
void rmt_update_position(struct rmt_figure *figure);
void rmt_update_thickness(struct rmt_figure *figure);
void rmt_update_colorcycle(struct rmt_figure *figure);
void rmt_render(const struct rmt_figure *figure);
enum rmt_alpha_modes rmt_get_alpha_mode(const struct rmt_figure *figure);
enum rmt_alpha_modes rmt_set_alpha_mode(struct rmt_figure *figure,
                                        enum rmt_alpha_modes amode);
float rmt_get_speed(const struct rmt_figure *figure);
float rmt_set_speed(struct rmt_figure *figure, float speed);
float rmt_change_speed(struct rmt_figure *figure, float spd_delta);
float rmt_set_default_speed(struct rmt_figure *figure);
Color rmt_get_color(const struct rmt_figure *figure);
Color rmt_set_color(struct rmt_figure *figure, Color color);
Color rmt_set_default_color(struct rmt_figure *figure);
bool rmt_colorcycle_active(const struct rmt_figure *figure);
bool rmt_toggle_colorcycle(struct rmt_figure *figure);
enum rmt_thick_modes rmt_get_thick_mode(const struct rmt_figure *figure);
enum rmt_thick_modes rmt_set_thick_mode(struct rmt_figure *figure,
                                        enum rmt_thick_modes tmode);
bool rmt_autothick_active(const struct rmt_figure *figure);
bool rmt_toggle_autothick(struct rmt_figure *figure);
float rmt_get_thick_control_delta(const struct rmt_figure *figure);
float rmt_change_thick_control_delta(struct rmt_figure *figure, float tcd_delta);
float rmt_set_default_thick_control_delta(struct rmt_figure *figure);
float rmt_get_thickness(const struct rmt_figure *figure);
float rmt_set_thickness(struct rmt_figure *figure, float thickness);
float rmt_thickness_up(struct rmt_figure *figure);
float rmt_thickness_down(struct rmt_figure *figure);
float rmt_set_default_thickness(struct rmt_figure *figure);

#endif